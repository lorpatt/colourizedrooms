﻿namespace ColouredRoomsWithInput.Models
{
    public class PixelDetails
    {
        // character
        public char PixelChar { get; set; }

        public PixelDetails(char inputChar = ' ')
        {
            PixelChar = inputChar;
        }
    }
}
