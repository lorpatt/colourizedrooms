﻿using ColouredRoomsWithInput.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ColouredRoomsWithInput.Business
{
    public class RoomColouring
    {
        PixelDetails[,] _floorPlan;
        int _floorLength;
        int _floorWidth;
        char _charToken = 'a';

        public RoomColouring(string input)
        {
            var rows = InitializeFloorDimensions(input);
            
            _floorPlan = new PixelDetails[_floorLength, _floorWidth];

            SetUpInitialFloorPlan(rows);

            

            // Print out the floor plan
            PrintFloorPlan();
        }

        private List<string> InitializeFloorDimensions(string input)
        {
            // split into rows
            var rows = input.Split('\n').ToList();

            // check that last row actually has walls in it
            if (string.IsNullOrWhiteSpace(rows.Last()))
                rows.RemoveAt(rows.Count - 1);

            // set class variables for floorplan's length and width
            _floorLength = rows.Count;
            _floorWidth = 0;
            rows.ForEach(row =>
            {
                if (row.Length > _floorWidth)
                    _floorWidth = row.Length;
            });

            return rows;
        }

        private void SetUpInitialFloorPlan(List<string> rows)
        {
            for (int l = 0; l < _floorLength; l++)
            {
                for (int w = 0; w < rows[l].Length; w++)
                {
                    var inputchar = rows[l][w];
                    
                    if (char.IsWhiteSpace(inputchar))
                        _floorPlan[l,w] = new PixelDetails();
                    else
                        _floorPlan[l,w] = new PixelDetails(inputchar);
                }
                if(rows[l].Length < _floorWidth)
                {
                    for (int i = rows[l].Length; i<_floorWidth; i++)
                    {
                        _floorPlan[l, i] = new PixelDetails();
                    }
                }
            }
            FindAndSetDoors();
            TokenizeRooms();
        }

        private void FindAndSetDoors()
        {
            for (int l = 0; l < _floorLength; l++)
            {
                for (int w = 0; w < _floorWidth; w++)
                {
                    if (!IsWall(l, w) && !IsCornerOfFloorPlan(l,w) && (IsOuterDoor(l, w) || IsInnerDoor(l, w)))
                    {
                        _floorPlan[l, w].PixelChar = '$';
                    }
                }
            }
        }

        private ConsoleColor GetBackgroundColour(char token)
        {
            switch (token)
            {
                case 'a':
                    return ConsoleColor.Blue;
                case 'b':
                    return ConsoleColor.DarkMagenta;
                case 'c':
                    return ConsoleColor.Green;
                case 'd':
                    return ConsoleColor.Red;
                case 'e':
                    return ConsoleColor.DarkCyan;
                case 'f':
                    return ConsoleColor.Gray;
                case 'g':
                    return ConsoleColor.DarkRed;
                case 'h':
                    return ConsoleColor.Yellow;
                case 'i':
                    return ConsoleColor.Cyan;
                default:
                    throw new Exception("There are too many rooms for this version of Room Colouring");
                    
            }
    }


        private void TokenizeRooms()
        {
 
            // to be inside a room you must be past a wall on l and w 
            for (int l = 0; l < _floorLength; l++)
            {
                bool pastWallX = false;
                for (int w = 0; w < _floorWidth; w++)
                {
                    if (IsWall(l, w))
                        pastWallX = true;
                    // make sure we got into room first
                    if(char.IsWhiteSpace(_floorPlan[l, w].PixelChar) && pastWallX)
                    {
                        if(FillInRoom(_charToken, l, w))
                            _charToken++;
                    }
                    
                }
            }
        }

        private bool FillInRoom(char token, int l, int w)
        {
            var indexMaxY = _floorLength - 1;
            var indexMaxX = _floorWidth - 1;
            // we are not in a room we are outside the building
            if (w==indexMaxX)
                return false;
            var endX = w;
            int startx = w;
            while(char.IsWhiteSpace(_floorPlan[l, ++endX].PixelChar))
            {
                // we are not in a room, we are outside the building
                if (endX == _floorWidth)
                    return false;
                CheckAndPopulateNextRow((l+1), w, endX, token);
            }
            for (int x = w; x< endX; x++)
            {
                _floorPlan[l, x].PixelChar = token;
            }

            return true;
        }


        private void CheckAndPopulateNextRow(int y, int previousStartX, int previousEndX, char token)
        {
            var firstOverlappingColumn = -1;
            for (int w = previousStartX; w < previousEndX; w++)
            {
                if (char.IsWhiteSpace(_floorPlan[y, w].PixelChar))
                {
                    firstOverlappingColumn = w;
                    break;
                }
            }
            // no columns in this row overlapped with the previous row
            if (firstOverlappingColumn<0)
                return;
            var newX = firstOverlappingColumn;
            // find first item in this room
            while(newX > 0 && char.IsWhiteSpace(_floorPlan[y, --newX].PixelChar))
            {
                if (newX == 0)
                    throw new IndexOutOfRangeException("The floor plan is too complex for this little program, we seem to be outside the building");
            }
            // find the end wall of this row item in this room
            var endX = firstOverlappingColumn;
            while (endX < _floorWidth && char.IsWhiteSpace(_floorPlan[y, ++endX].PixelChar))
            {
                if (endX == _floorWidth)
                    throw new IndexOutOfRangeException("The floor plan is too complex for this little program, we seem to be outside the building");
            }

            for (int i = newX+1; i < endX; i++)
            {
                _floorPlan[y, i].PixelChar = token;
                CheckAndPopulateNextRow((y + 1), newX, endX, token);
                if (y-1>0)
                    CheckAndPopulateNextRow((y - 1), newX, endX, token);
            }
        }

        private bool IsOuterDoor(int y, int x)
        {
            var indexMaxY = _floorLength - 1;
            var indexMaxX = _floorWidth - 1;

            return ((x == 0 && IsWall(y - 1, x) && IsWall(y + 1, x) && !IsWall(y, x + 1)) ||
                (x == indexMaxX && IsWall(y - 1, x) && IsWall(y + 1, x) && !IsWall(y, x - 1)) ||
                (y == 0 && IsWall(y, x - 1) && IsWall(y, x + 1) && !IsWall(y + 1, x)) ||
                (y == indexMaxY && IsWall(y, x - 1) && IsWall(y, x + 1) && !IsWall(y - 1, x)));
        }

        private bool IsInnerDoor(int y, int x)
        {
            var indexMaxY = _floorLength - 1;
            var indexMaxX = _floorWidth - 1;
            if (x == 0 || x == indexMaxX || y == 0 || y == indexMaxY)
                return false;
            return (IsWall(y, x + 1) && IsWall(y, x - 1) && !IsWall(y + 1, x) && !IsWall(y - 1, x)) ||
                (IsWall(y + 1, x) && IsWall(y - 1, x) && !IsWall(y, x + 1) && !IsWall(y, x - 1));
        }
        private bool IsCornerOfFloorPlan(int y, int x)
        {
            var indexMaxY = _floorLength - 1;
            var indexMaxX = _floorWidth - 1;
            return (x == 0 && y == 0) || (x == indexMaxX && y == indexMaxY) ||
                (x == 0 && y == indexMaxY) || (x == indexMaxX && y == 0);
        }

        private bool IsWall(int y, int x)
        {
            return _floorPlan[y, x].PixelChar=='#';
        }

        private void PrintFloorPlan()
        {
            for (int l = 0; l < _floorLength; l++)
            {
                for (int w = 0; w < _floorWidth; w++)
                {
                    var item = _floorPlan[l,w];
                    if (item.PixelChar == '$' || item.PixelChar == ' ')
                    {
                        // $ is a door; ' ' is an area outside the building
                        item.PixelChar = ' ';
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    else if (item.PixelChar == '#')
                    {
                        // this is a wall
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    else
                    {
                        Console.BackgroundColor = GetBackgroundColour(item.PixelChar);
                        item.PixelChar = ' ';
                    }

                    Console.Write(item.PixelChar);
                }
                Console.WriteLine();
            }
        }
    }
}
