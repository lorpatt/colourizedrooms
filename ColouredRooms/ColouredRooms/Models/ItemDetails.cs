﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ColouredRooms.Models
{

    public class ItemDetails
    {
        public ConsoleColor BackgroundColour { get; set; }
        public char Char { get; private set; }
        public ItemDetails(ConsoleColor background, bool isWall = false)
        {
            BackgroundColour = background;
            Char = isWall ? '#' : ' ';
        }

        public void ResetChar(bool isWall = false)
        {
            Char = isWall ? '#' : ' ';
        }

    }
}
