﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ColouredRooms.Models
{
    public class Room
    {
        public ConsoleColor FloorColour { get; set; }
        public ConsoleColor WallColour { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public Coordinates AbsoluteCoordinates { get; set; }
        public List<Coordinates> RelativeDoorCoord { get; set; }

        public Room(ConsoleColor floorColour, int x, int y, int length, int width)
        {
            WallColour = ConsoleColor.Black;
            FloorColour = floorColour;
            AbsoluteCoordinates = new Coordinates(x, y);
            Length = length;
            Width = width;
            RelativeDoorCoord = new List<Coordinates>();
        }
    }
}
