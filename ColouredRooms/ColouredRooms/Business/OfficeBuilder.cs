﻿using ColouredRooms.Models;
using System;
using System.Collections.Generic;

namespace ColouredRooms.Business
{
    public class OfficeBuilder
    {
        List<List<ItemDetails>> _officeBuilding;

        public OfficeBuilder()
        {
            _officeBuilding = new List<List<ItemDetails>>();

            // Set up basic room
            SetUpMainRoom(16, 26, ConsoleColor.Cyan);
            SetUpInnerRooms();

            // Print out the floor plan
            PrintFloorPlan();

        }

        private void SetUpMainRoom(int length, int width, ConsoleColor roomColor)
        {
            for (int l = 0; l < length; l++)
            {
                var row = new List<ItemDetails>();
                for (int w = 0; w < width; w++)
                {
                    
                    if (l == 0 || l == length-1 || w == 0|| w== width - 1)
                    {
                        row.Add(new ItemDetails(ConsoleColor.Black, true));
                    }
                    else
                    {
                        row.Add(new ItemDetails(roomColor));
                    }
                }
                _officeBuilding.Add(row);
            }
        }

        private void SetUpInnerRooms()
        {
            Room room1 = new Room(ConsoleColor.Magenta, 0, 0, 5, 10);
            room1.RelativeDoorCoord.Add(new Coordinates(9, 2));

            AddRoomToFloorPlan(room1);

            Room room2 = new Room(ConsoleColor.DarkBlue, 0, 4, 6, 10);
            room2.RelativeDoorCoord.Add(new Coordinates(9, 2));

            AddRoomToFloorPlan(room2);

            Room room3 = new Room(ConsoleColor.Red, 0, 9, 7, 16);
            room3.RelativeDoorCoord.Add(new Coordinates(12, 0));
            room3.RelativeDoorCoord.Add(new Coordinates(13, 0));

            AddRoomToFloorPlan(room3);

            Room room4 = new Room(ConsoleColor.DarkYellow, 14, 0, 8, 12);
            room4.RelativeDoorCoord.Add(new Coordinates(5, 7));
            room4.RelativeDoorCoord.Add(new Coordinates(6, 7));

            AddRoomToFloorPlan(room4);

        }

        private void AddRoomToFloorPlan(Room room)
        {
            var absLength = room.AbsoluteCoordinates.Y + room.Length;
            var absWidth = room.AbsoluteCoordinates.X + room.Width;
            for (int y = room.AbsoluteCoordinates.Y; y< absLength; y++)
            {
                for (int x = room.AbsoluteCoordinates.X; x<absWidth; x++)
                {
                    if (IsEdge(room, new Coordinates(x, y)))
                    {
                        _officeBuilding[y][x].BackgroundColour = ConsoleColor.Black;
                        _officeBuilding[y][x].ResetChar(true);
                    }                  
                    else
                    {
                        _officeBuilding[y][x].BackgroundColour = room.FloorColour;
                        _officeBuilding[y][x].ResetChar();
                    }
                }
            }
            room.RelativeDoorCoord.ForEach(d =>
            {
                var door = _officeBuilding[room.AbsoluteCoordinates.Y + d.Y][room.AbsoluteCoordinates.X + d.X];
                door.BackgroundColour = ConsoleColor.Black;
                door.ResetChar();
            });
        }

        private bool IsEdge(Room room, Coordinates coord) 
        {
            var absLengthCoord = room.Length + room.AbsoluteCoordinates.Y;
            var absWidthCoord = room.Width + room.AbsoluteCoordinates.X;
            return (coord.X == room.AbsoluteCoordinates.X ||
                coord.X == absWidthCoord - 1 ||
                coord.Y == room.AbsoluteCoordinates.Y ||
                coord.Y == absLengthCoord - 1);
        }

        private void PrintFloorPlan()
        {

            for(int i = 0; i < _officeBuilding.Count; i++)
            {
                for (int j = 0; j < _officeBuilding[i].Count; j++)
                {
                    var item = _officeBuilding[i][j];
                    Console.BackgroundColor = item.BackgroundColour;
                    Console.Write(item.Char);
                }
                Console.WriteLine();
            }
        }
    }
}
