﻿using System;
using ColouredRoomsWithInput.Business;

namespace ColouredRoomsWithInput
{
    class Program
    {
        static void Main(string[] args)
        {
            string input =
                 "##########\n"
                + "#   #    #\n"
                + "#   #    #\n"
                + "## #### ##\n"
                + "#        #\n"
                + "#        #\n"
                + "#  #######\n"
                + "#  #  #  #\n"
                + "#        #\n"
                + "##########";

            string input2 =
                 "###########\n"
                + "#   #     #\n"
                + "#   #     #\n"
                + "## #### ###\n"
                + "#        #\n"
                + "#        #\n"
                + "#   #######\n"
                + "#   #  #  #\n"
                + "#         #\n"
                + "###########";

            string input3 =
                  "###########\n"
                + "#    #    #\n"
                + "#    #    #\n"
                + "### #### ##\n"
                + " #        #\n"
                + " #        #\n"
                + " #  #######\n"
                + " #  #  #  #\n"
                + " #        #\n"
                + " ##########";

            string input4 =
                  "###########\n"
                + "#    #    #\n"
                + "#    #    #\n"
                + "### #### ##\n"
                + "#   #     #\n"
                + "#   #     #\n"
                + "#   #  #  #\n"
                + "#   #  ####\n"
                + "#         #\n"
                + "###########";

            string input5 =
                  "###########\n"
                + "#    #    #\n"
                + "#    #    #\n"
                + "### #### ##\n"
                + "#   #  #  #\n"
                + "#   #  #  #\n"
                + "#   #  #  #\n"
                + "#         #\n"
                + "#         #\n"
                + "###########";
            try
            {
                new RoomColouring(input);
                new RoomColouring(input2);
                new RoomColouring(input3);
                new RoomColouring(input4);
                new RoomColouring(input5);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Recieved error while running program {ex.InnerException}");
            }

            Console.WriteLine("Press any key to close window.");
            Console.ReadKey();
        }
    }
}
